from django.urls import path
from .views import list_of_products, product_details, create_product, update_product

urlpatterns = [
    path("", list_of_products, name = "home"),
    path("<int:pk>/", product_details, name = "details"),
    path("create/", create_product, name = "create"), 
    path("<int:pk>/update/", update_product, name = "update")
]
