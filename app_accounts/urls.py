from django.urls import path  # type: ignore
from .views import user_login, user_logout, signup  # type: ignore


urlpatterns = [
    path("login/", user_login.as_view(), name="login"),
    path("logout/", user_logout.as_view(), name="logout"),
    path("signup/", signup, name="signup"),
]
