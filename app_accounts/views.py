from django.shortcuts import render, redirect  # type: ignore
from django.contrib.auth.views import LoginView, LogoutView  # type: ignore
from django.contrib.auth.forms import UserCreationForm  # type: ignore
from django.contrib.auth import login, authenticate  # type: ignore

# Create your views here.
class user_login(LoginView):
    template_name = "registration/login.html"
    
class user_logout(LogoutView):
    template_name = "registration/logout.html"
    
def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()
    return render(request, "registration/signup.html", {"form": form})
