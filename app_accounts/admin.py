from django.contrib import admin

from app_accounts.models import Customer
from .category import Category
# Register your models here.
admin.site.register(Category)
admin.site.register(Customer)