from django import forms
from .models import OrderItems


class OrderItemsForm(forms.ModelForm):
    class Meta:
        model = OrderItems
        exclude = []