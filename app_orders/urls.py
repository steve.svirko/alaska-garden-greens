from django.urls import path
from .views import your_orders


urlpatterns = [
    path("", your_orders, name = "orders"),
]
