from django.shortcuts import render, redirect
from .models import OrderItems
from .forms import OrderItemsForm
# Create your views here.


def your_orders(request):
    context = {}
    if request.method == 'POST':
        form = OrderItemsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = OrderItemsForm
        context['form'] = form
    return render(request, "orders/your_order.html", context)