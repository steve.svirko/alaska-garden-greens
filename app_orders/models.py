
from django.db import models
from django.contrib.auth.models import User
from multiselectfield import MultiSelectField
from app_main.models import Produce
# Create your models here.



class OrderItems(models.Model):
    purchaser = models.ForeignKey(
        User, related_name="orderitems", on_delete=models.CASCADE, null=True
    )
    PRODUCE_CHOICES = [
        ('arugula', 'arugula'), 
        ('spinach', 'spinach'),
        ('mustard', 'mustard'),
        ('lettuce', 'lettuce'),
    ]
    items = MultiSelectField(choices = PRODUCE_CHOICES, max_length = 30)
    # number_of_items = models.IntegerField()
    total = models.DecimalField(decimal_places=3, max_digits=10, blank=True)
    
    LOCATION_CHOICES = [
        ('South Anchorage farmers market', 'South Anchorage farmers market saturday 9 am - 2pm'),
        ('Farm where I store my produce', 'Farm where I store my produce friday 2-3pm'),
    ]
    locations = models.CharField(choices=LOCATION_CHOICES, max_length = 50)
    def __str__(self):
        return self.total

# class Numberofitems(models.Model):
#     order_number = models.ManyToManyField(OrderItems, related_name = "items")
    
#     def __str__(self):
#         return self.order_number