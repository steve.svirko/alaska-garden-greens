# Generated by Django 4.1 on 2022-08-09 21:39

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderItems',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('items', models.PositiveIntegerField(default=1)),
                ('total', models.DecimalField(blank=True, decimal_places=3, max_digits=10)),
                ('locations', models.CharField(blank=True, max_length=50)),
                ('purchaser', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='orderitem', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
