## list of steps 
 * [x]  clone repository
 * [x]  create virtual environment 
 * [x]  create super user
 * [x]  1. register apps
 * [x]  2. in acounts create create urls page, views, and templates for login
 * [ ]  3. in main create models, forms, urls, views and templates for viewing lists of items and viewing each individual item maybe see how much is available
 * [ ]  4. in receipts create a view page that shows all receipts and a page that shows the details of each item and filters by the user
 * [ ]  5. in orders app allow someone to choose what they want to order and in what quantity, shows final price and where they can pick up and what time
 * [ ]  6. create app called admin that allows the loged in admin to see all orders and the names of who ordered them and what time they want to pick up.

## step 1
 * [x] 1. register apps 


## step 2 
 * [x] in acounts create create urls page, views, and templates for login
 * [x] create urls page 
 * [x] register all accounts urls
 * [x] in views create needed views, signup, login and logout as needed
 * [x] create templates folder
 * [x] create folder in templates called registration
 * [x] create login, logout and signup templates in the registration folder

## step 3

* [ ]  3. in main create models, forms, urls, views and templates for viewing lists of items and viewing each individual item maybe see how much is available
* [x]  create models for items that can be sold don't forget to return __str__ self.name
* [ ]  create forms
* [x]  create view that lists all items
* [x]  create view that gives details of each item
* [x]  create templates for the views
* [ ]  create link to the order page
* [ ]  a create view that allows admin to create new items

## step 4 

* [ ]  4. in receipts create a view page that shows all receipts and a page that shows the details of each item and filters by the user
* [ ]  create a page lists all the receipts for the orders the user has made
* [ ]  create a page that shows the details of each receipt

## step 5
 * [ ]  5. in orders app allow someone to choose what they want to order and in what quantity, shows final price and where they can pick up and what time
 * [ ]  from the view page add a link to order
 * [ ]  in the order show each item and the quatities of each item
 * [ ]  allow user to select desired quantity 
 * [ ]  show user how much it is going to cost
 * [ ]  show user where and what time they can pick up their items

## step 6 
 * [ ]  6. create app called admin that allows the loged in admin to see all orders and the names of who ordered them and what time they want to pick up.
 * [ ]  in the admin allow updating of quantities
 * [ ]  see all orders and the names of who made them
 * [ ]  list orders by where they want to be picked up i.e saturday market or headquarters
 * [ ] 